#
# MAINTAINER      zhangyansong <zys518@gmail.com>
# DOCKER-VERSION  1.0
# Dockerizing CentOS7.1: Dockerfile for building CentOS images
#
FROM       centos:centos7.1.1503
MAINTAINER zhangyansong <zys518@gmail.com>

ENV TZ "Asia/Shanghai"
ENV TERM xterm

